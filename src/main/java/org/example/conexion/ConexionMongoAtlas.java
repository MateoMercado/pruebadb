package org.example.conexion;

import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.InsertOneOptions;
import com.mongodb.MongoException;
import org.bson.Document;

public class ConexionMongoAtlas {
  //uso
  public static void main(String[] args) {
    String connectionString = "mongodb+srv://user:Admin123@projectdberror404.xk14nnd.mongodb.net/";

    try {
      MongoClient mongoClient = MongoClients.create(connectionString);

      // Crear la base de datos
      MongoDatabase database = mongoClient.getDatabase("toDoList");
      System.out.println("Base de datos 'toDoList' creada correctamente.");

      // Crear la colección
      database.createCollection("users");
      System.out.println("Colección 'users' creada correctamente.");

      // Obtener la colección
      MongoCollection<Document> collection = database.getCollection("users");

      // Insertar usuarios
      Document user1 = new Document()
          .append("correo", "usuario1@example.com")
          .append("nombre", "Juan")
          .append("apellido", "Pérez")
          .append("nombreUsuario", "juanito")
          .append("contrasena", "clave123");

      Document user2 = new Document()
          .append("correo", "usuario2@example.com")
          .append("nombre", "María")
          .append("apellido", "Gómez")
          .append("nombreUsuario", "mariag")
          .append("contrasena", "clave456");

      collection.insertOne(user1, new InsertOneOptions());
      collection.insertOne(user2, new InsertOneOptions());

      System.out.println("Usuarios agregados correctamente.");

      mongoClient.close();
    } catch (MongoException e) {
      System.out.println("Error de conexión: " + e.getMessage());
    }
  }
  //prueba de conexion
//  public static void main(String[] args) {
//    String connectionString = "mongodb+srv://user:Admin123@projectdberror404.xk14nnd.mongodb.net/";
//    try {
//      MongoClient mongoClient = MongoClients.create(connectionString);
//      System.out.println("Conexión exitosa");
//      mongoClient.close(); // Cierra la conexión cuando ya no la necesitas
//    } catch (MongoException e) {
//      System.out.println("Error de conexión: " + e.getMessage());
//    }
//  }
}





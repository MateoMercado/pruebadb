package org.example.delete;

import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoDatabase;
import com.mongodb.MongoException;

public class EliminarColeccion {

  public static void eliminarColeccion() {
    String connectionString = "mongodb+srv://user:Admin123@projectdberror404.xk14nnd.mongodb.net/";

    try {
      MongoClient mongoClient = MongoClients.create(connectionString);
      MongoDatabase database = mongoClient.getDatabase("toDoList");

      database.getCollection("users").drop(); // Elimina la colección "users"

      System.out.println("Colección 'users' eliminada correctamente.");
      mongoClient.close();
    } catch (MongoException e) {
      System.out.println("Error al eliminar la colección: " + e.getMessage());
    }
  }
}


package org.example.delete;

import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoCollection;
import com.mongodb.MongoException;
import org.bson.Document;

public class EliminarDocumentacion {

  public static void eliminarUsuarios() {
    String connectionString = "mongodb+srv://user:Admin123@projectdberror404.xk14nnd.mongodb.net/";

    try {
      MongoClient mongoClient = MongoClients.create(connectionString);
      MongoDatabase database = mongoClient.getDatabase("toDoList");
      MongoCollection<Document> collection = database.getCollection("users");

      collection.deleteMany(new Document()); // Elimina todos los documentos en la colección

      System.out.println("Usuarios eliminados correctamente.");
      mongoClient.close();
    } catch (MongoException e) {
      System.out.println("Error al eliminar usuarios: " + e.getMessage());
    }
  }
}


package org.example.delete;

import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoClient;
import com.mongodb.MongoException;

public class EliminarDataBase {

  public static void eliminarBaseDeDatos() {
    String connectionString = "mongodb+srv://user:Admin123@projectdberror404.xk14nnd.mongodb.net/";

    try {
      MongoClient mongoClient = MongoClients.create(connectionString);
      mongoClient.getDatabase("toDoList").drop(); // Elimina la base de datos "toDoList"

      System.out.println("Base de datos 'toDoList' eliminada correctamente.");
      mongoClient.close();
    } catch (MongoException e) {
      System.out.println("Error al eliminar la base de datos: " + e.getMessage());
    }
  }
}


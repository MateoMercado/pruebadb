package org.example.conexion;

import org.example.delete.EliminarColeccion;
import org.example.delete.EliminarDataBase;
import org.example.delete.EliminarDocumentacion;

public class Delete {

  public static void main(String[] args) {
    EliminarDocumentacion.eliminarUsuarios();
    EliminarColeccion.eliminarColeccion();
    EliminarDataBase.eliminarBaseDeDatos();
  }

}

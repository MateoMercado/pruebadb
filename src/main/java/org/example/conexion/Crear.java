package org.example.conexion;

import org.example.crear.CrearColeccion;
import org.example.crear.CrearDataBase;
import org.example.crear.CrearDocumentacion;

public class Crear {

  public static void main(String[] args) {
    CrearDataBase.crearBaseDeDatos();
    CrearColeccion.crearColeccion();
    CrearDocumentacion.crearUsuarios();
  }

}

package org.example.crear;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;
import com.mongodb.MongoException;

public class CrearDataBase {

  public static void crearBaseDeDatos() {
    String connectionString = "mongodb+srv://user:Admin123@projectdberror404.xk14nnd.mongodb.net/";

    try {
      MongoClient mongoClient = MongoClients.create(connectionString);
      MongoDatabase database = mongoClient.getDatabase("toDoList");
      System.out.println("Base de datos 'toDoList' creada correctamente.");
      mongoClient.close();
    } catch (MongoException e) {
      System.out.println("Error al crear la base de datos: " + e.getMessage());
    }
  }
}

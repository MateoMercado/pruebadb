package org.example.crear;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;
import com.mongodb.MongoException;

public class  CrearColeccion {

  public static void crearColeccion() {
    String connectionString = "mongodb+srv://user:Admin123@projectdberror404.xk14nnd.mongodb.net/";

    try {
      MongoClient mongoClient = MongoClients.create(connectionString);
      MongoDatabase database = mongoClient.getDatabase("toDoList");
      database.createCollection("users");
      System.out.println("Colección 'users' creada correctamente.");
      mongoClient.close();
    } catch (MongoException e) {
      System.out.println("Error al crear la colección: " + e.getMessage());
    }
  }
}

